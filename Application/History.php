<?php


namespace NimServer\Application;

/**
 * 历史记录
 * Class History
 * @package NimServer\Application
 */
class History extends Base
{
    /**
     * @param $from 发送者accid
     * @param $to   接收者accid
     * @param $begintime    开始时间，毫秒级
     * @param $endtime  截止时间，毫秒级
     * @param int $limit    本次查询的消息条数上限(最多100条),小于等于0，或者大于100，会提示参数错误
     * @param int $reverse  1按时间正序排列，2按时间降序排列。其它返回参数414错误.默认是按降序排列，即时间戳最晚的消息排在最前面。
     * @param string $type  查询指定的多个消息类型，类型之间用","分割，不设置该参数则查询全部类型消息格式示例： 0,1,2,3
    类型支持： 1:图片，2:语音，3:视频，4:地理位置，5:通知，6:文件，10:提示，11:Robot，100:自定义
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function querySessionMsg($from, $to, $begintime, $endtime, $limit = 10, $reverse = 1, $type = '')
    {
        $data = [
            'from' => $from,
            'to' => $to,
            'begintime' => $begintime,
            'endtime' => $endtime,
            'limit' => $limit
        ];
        if ($reverse) $data['reverse'] = $reverse;
        if ($type !== '') $data['type'] = $type;
        return $this->httpCurl('history/querySessionMsg.action', $data);
    }
}