<?php


namespace NimServer\Application;
/**
 * 用户关系托管
 * Class Friend
 * @package NimServer\Application
 */
class Friend extends Base
{
    /**
     * 加好友
     * @param string $accId 加好友发起者accid
     * @param string $faccId 加好友接收者accid
     * @param int $type 1直接加好友，2请求加好友，3同意加好友，4拒绝加好友
     * @param string $msg 加好友对应的请求消息，第三方组装，最长256字符
     * @param string $serverex 服务器端扩展字段，限制长度256 此字段client端只读，server端读写
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function add($accId, $faccId, $type, $msg = '', $serverEx = '')
    {
        return $this->httpCurl('friend/add.action', [
            'accid'    => $accId,
            'faccid'   => $faccId,
            'type'     => $type,
            'msg'      => $msg,
            'serverex' => $serverEx
        ]);
    }

    /**
     * 更新好友相关信息
     * @param string $accId 发起者accid
     * @param string $faccId 要修改朋友的accid
     * @param string $alias 给好友增加备注名，限制长度128，可设置为空字符串
     * @param string $ex 修改ex字段，限制长度256，可设置为空字符串
     * @param string $serverEx 修改serverex字段，限制长度256，可设置为空字符串 此字段client端只读，server端读写
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function update($accId, $faccId, $alias = '', $ex = '', $serverEx = '')
    {
        return $this->httpCurl('friend/update.action', [
            'accid'    => $accId,
            'faccid'   => $faccId,
            'alias'    => $alias,
            'ex'       => $ex,
            'serverex' => $serverEx
        ]);
    }

    /**
     * 删除好友
     * @param string $accId 发起者accid
     * @param string $faccId 要删除朋友的accid
     * @param bool $isDeleteAlias 是否需要删除备注信息 默认false:不需要，true:需要
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function delete($accId, $faccId, bool $isDeleteAlias)
    {
        return $this->httpCurl('friend/delete.action', [
            'accid'         => $accId,
            'faccid'        => $faccId,
            'isDeleteAlias' => $isDeleteAlias
        ]);
    }

    /**
     * 获取好友关系
     * 查询某时间点起到现在有更新的双向好友
     * @param string $accId 发起者accid
     * @param int $updateTime 更新时间戳，接口返回该时间戳之后有更新的好友列表
     * @param int $createTime 【Deprecated】定义同updatetime
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function get(string $accId, int $updateTime, int $createTime)
    {
        return $this->httpCurl('friend/get.action', [
            'accid'      => $accId,
            'updatetime' => $updateTime,
            'createtime' => $createTime
        ]);
    }

    /**
     * 设置黑名单/静音
     * @param string $accId 用户帐号，最大长度32字符，必须保证一个APP内唯一
     * @param string $targetAcc 被加黑或加静音的帐号
     * @param int $relationType 本次操作的关系类型,1:黑名单操作，2:静音列表操作
     * @param int $value 操作值，0:取消黑名单或静音，1:加入黑名单或静音
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function setSpecialRelation(string $accId, string $targetAcc, int $relationType, int $value)
    {
        return $this->httpCurl('friend/setSpecialRelation.action', [
            'accid'        => $accId,
            'targetAcc'    => $targetAcc,
            'relationType' => $relationType,
            'value'        => $value
        ]);
    }

    // 查看指定用户的黑名单和静音列表
    public function listBlackAndMuteList(string $accId)
    {
        return $this->httpCurl('friend/listBlackAndMuteList.action', [
            'accid'        => $accId
        ]);
    }
}