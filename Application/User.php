<?php


namespace NimServer\Application;

class User extends Base
{

    // 创建网易云通信ID
    public function create($accId, array $data = [])
    {
        $data   = array_merge([
            'accid' => $accId
        ], $data);
        $result = $this->httpCurl('user/create.action', $data);
        return $result['info'];
    }

    // 更新网易云通信token
    public function updateToken($accId, $token = '')
    {
        return $this->httpCurl('user/update.action', [
            'accid' => $accId,
            'token' => $token
        ]);
    }

    // 重置网易云通信token
    public function refreshToken($accId)
    {
        return $this->httpCurl('user/refreshToken.action', [
            'accid' => $accId
        ]);
    }

    // 封禁网易云通信ID
    public function block($accId, $needKick = false, $kickNotifyExt = '')
    {
        return $this->httpCurl('user/block.action', [
            'accid'         => $accId,
            'needkick'      => $needKick,
            'kickNotifyExt' => $kickNotifyExt
        ]);
    }

    // 解禁网易云通信ID
    public function unBlock($accId)
    {
        return $this->httpCurl('user/unblock.action', [
            'accid' => $accId
        ]);
    }

    /**
     * 更新用户名片
     * @param $accId
     * @param array $data name icon sign email birth mobile gender ex
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function updateUInfo($accId, array $data = [])
    {
        return $this->httpCurl('user/updateUinfo.action', array_merge([
            'accid' => $accId
        ], $data));
    }

    /**
     * 获取用户名片
     * @param array $accIds 用户帐号（例如：JSONArray对应的accid串，如：["zhangsan"]，如果解析出错，会报414）（一次查询最多为200）
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function getUInfos(array $accIds = [])
    {
        return $this->httpCurl('user/getUinfos.action', [
            'accids' => json_encode($accIds)
        ]);
    }

    /**
     * 设置桌面端在线时，移动端是否需要推送
     * @param $accId
     * @param bool $donnopOpen 桌面端在线时，移动端是否不推送：true:移动端不需要推送，false:移动端需要推送
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function setDonnop($accId, bool $donnopOpen)
    {
        return $this->httpCurl('user/setDonnop.action', [
            'accid'      => $accId,
            'donnopOpen' => $donnopOpen
        ]);
    }

    /**
     * 账号全局禁言
     * @param $accId
     * @param bool $mute 是否全局禁言：true：全局禁言，false:取消全局禁言
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function mute($accId, bool $mute)
    {
        return $this->httpCurl('user/setDonnop.action', [
            'accid' => $accId,
            'mute'  => $mute
        ]);
    }

    /**
     * 账号全局禁用音视频
     * @param $accId
     * @param bool $mute 是否全局禁言：true：全局禁言，false:取消全局禁言
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function muteAv($accId, bool $mute)
    {
        return $this->httpCurl('user/setDonnop.action', [
            'accid' => $accId,
            'mute'  => $mute
        ]);
    }
}