<?php

namespace NimServer\Application;

/**
 * 消息功能
 * Class Msg
 * @package NimServer\Application
 */
class Msg extends Base
{
    /**
     * 发送普通消息
     * @param string $from 发送者accid，用户帐号，最大32字符，必须保证一个APP内唯一
     * @param string $to ope==0是表示accid即用户id，ope==1表示tid即群id
     * @param array $msg 消息体
     * @param int $ope 0：点对点个人消息，1：群消息（高级群），其他返回414
     * @param array $extra
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function sendMsg(string $from, string $to, array $msg, int $ope = 0, array $extra = [])
    {
        $data = array_merge($extra, [
            'from' => $from,
            'to'   => $to,
            'ope'  => $ope
        ], $msg);
        return $this->httpCurl('msg/sendMsg.action', $data);
    }

    /**
     * 发送自定义系统通知
     *
     * 1.自定义系统通知区别于普通消息，方便开发者进行业务逻辑的通知；
     * 2.目前支持两种类型：点对点类型和群类型（仅限高级群），根据msgType有所区别。
     * @param string $from 发送者accid，用户帐号，最大32字符，APP内唯一
     * @param string $to msgtype==0是表示accid即用户id，msgtype==1表示tid即群id
     * @param string|array $attach 自定义通知内容，第三方组装的字符串，建议是JSON串，最大长度4096字符
     * @param int $msgType 0：点对点自定义通知，1：群消息自定义通知，其他返回414
     * @param array $extra pushcontent/payload/sound/save/option/isForcePush/forcePushContent/forcePushAll/forcePushList
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function sendAttachMsg(string $from, string $to, $attach, int $msgType = 0, array $extra = [])
    {
        $attach = is_array($attach) ? json_encode($attach, 256) : $attach;
        $data   = array_merge($extra, [
            'from'    => $from,
            'to'      => $to,
            'msgtype' => $msgType,
            'attach'  => $attach
        ]);
        return $this->httpCurl('msg/sendAttachMsg.action', $data);
    }


    /**
     * 消息撤回
     * 消息撤回接口，可以撤回一定时间内的点对点与群消息
     * @param string $deleteMsgId 要撤回消息的msgid
     * @param int $timeTag 要撤回消息的创建时间
     * @param int $type 7:表示点对点消息撤回，8:表示群消息撤回，其它为参数错误
     * @param string $from 发消息的accid
     * @param string $to 如果点对点消息，为接收消息的accid,如果群消息，为对应群的tid
     * @param array $extra msg/ignoreTime/pushcontent/payload
     */
    public function recall(string $deleteMsgId, int $timeTag, int $type, string $from, string $to, array $extra = [])
    {

        $data = [
            'deleteMsgid' => $deleteMsgId,
            'timetag'     => $timeTag,
            'type'        => $type,
            'from'        => $from,
            'to'          => $to
        ];
        if ($extra) $data = array_merge($data, $extra);
        return $this->httpCurl('msg/recall.action', $data);
    }

    /**
     * 发送广播消息
     * @param string $body 广播消息内容，最大4096字符
     * @param string $from 发送者accid, 用户帐号，最大长度32字符，必须保证一个APP内唯一
     * @param bool $isOffline 是否存离线，true或false，默认false
     * @param float|int $ttl 存离线状态下的有效期，单位小时，默认7天
     * @param array $targetOs 目标客户端，默认所有客户端，jsonArray，格式：["ios","aos","pc","web","mac"]
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function broadcastMsg(string $body, string $from, bool $isOffline = false, int $ttl = 7 * 24, array $targetOs = [])
    {
        $data = [
            'body'      => $body,
            'from'      => $from,
            'isOffline' => $isOffline,
            'ttl'       => $ttl
        ];
        if ($targetOs) $data = array_merge($data, ['targetOs' => json_encode($targetOs, 256)]);
        return $this->httpCurl('msg/broadcastMsg.action', $data);
    }

    /**
     * 单向撤回消息
     * @param string $deleteMsgId 要撤回消息的msgid
     * @param int $timeTag 要撤回消息的创建时间
     * @param int $type 13:表示点对点消息撤回，14:表示群消息撤回，其它为参数错误
     * @param string $from 发消息的accid
     * @param string $to 如果点对点消息，为接收消息的accid,如果群消息，为对应群的tid
     * @param string $msg 可以带上对应的描述
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function delMsgOneWay(string $deleteMsgId, int $timeTag, int $type, string $from, string $to, string $msg = '')
    {
        return $this->httpCurl('msg/delMsgOneWay.action', [
            'deleteMsgid' => $deleteMsgId,
            'timetag'     => $timeTag,
            'type'        => $type,
            'from'        => $from,
            'to'          => $to,
            'msg'         => $msg
        ]);
    }

    /**
     * 删除会话漫游
     * @param int $type 会话类型，1-p2p会话，2-群会话，其他返回414
     * @param string $from 发送者accid, 用户帐号，最大长度32字节
     * @param string $to type=1表示对端accid，type=2表示tid
     * @return array|mixed
     * @throws \NimServer\Exception\YunXinBusinessException
     * @throws \NimServer\Exception\YunXinInnerException
     * @throws \NimServer\Exception\YunXinNetworkException
     */
    public function delRoamSession(int $type, string $from, string $to)
    {
        return $this->httpCurl('msg/delRoamSession.action', [
            'type' => $type,
            'from' => $from,
            'to'   => $to
        ]);
    }
}