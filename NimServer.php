<?php


namespace NimServer;


use think\helper\Str;

/**
 * Class NimServer
 * @method static \NimServer\Application\ChatRoom   chatRoom($appKey = '', $appSecret = '')
 * @method static \NimServer\Application\Friend     friend($appKey = '', $appSecret = '')
 * @method static \NimServer\Application\History    history($appKey = '', $appSecret = '')
 * @method static \NimServer\Application\Msg        msg($appKey = '', $appSecret = '')
 * @method static \NimServer\Application\SuperTeam  superTeam($appKey = '', $appSecret = '')
 * @method static \NimServer\Application\Team       team($appKey = '', $appSecret = '')
 * @method static \NimServer\Application\User       user($appKey = '', $appSecret = '')
 * @package NimServer
 */
class NimServer
{
    public static function __callStatic($name, $config = [])
    {
        $namespace = "\\NimServer\\Application\\" . Str::studly($name);
        $config    = array_merge(config('nimServer'), $config);
        return new $namespace($config['AppKey'], $config['AppSecret']);
    }
}