<?php


namespace NimServer\Lib;


class Message
{
    public static function text($content)
    {
        return [
            'type' => 0,
            'body' => json_encode(["msg" => $content])
        ];
    }

    public static function image()
    {

    }

    public static function audio()
    {

    }

    public static function video()
    {

    }

    public static function location()
    {

    }

    public static function file()
    {

    }

    public static function tip()
    {

    }

    public static function custom(array $data = [])
    {
        return [
            'type' => 100,
            'body' => json_encode($data)
        ];
    }

}